import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from datetime import datetime


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()

from attendees.models import AccountVO


def updateAccountVO(ch, method, properties, body):
    content = json.loads(body)
    first_name = content["first_name"]
    last_name = content["last_name"]
    email = content["email"]
    is_active = content["is_active"]
    updated = datetime.fromisoformat(content["updated"])

    if is_active:
        AccountVO.objects.filter(updated__lt=updated).update_or_create(
            email=email,
            defaults={
                "updated": updated,
                "first_name": first_name,
                "last_name": last_name,
            },
        )
    else:
        AccountVO.objects.filter(email=email).delete()


def main():
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()

    channel.exchange_declare(
        exchange="account_info",
        exchange_type="fanout",
    )

    result = channel.queue_declare(queue="", exclusive=True)
    queue_name = result.method.queue

    channel.queue_bind(exchange="account_info", queue=queue_name)

    channel.basic_consume(
        queue=queue_name,
        on_message_callback=updateAccountVO,
        auto_ack=True,
    )

    channel.start_consuming()


if __name__ == "__main__":
    while True:
        try:
            main()
        except KeyboardInterrupt:
            print("Interrupted")
            try:
                sys.exit(0)
            except SystemExit:
                os._exit(0)

        except AMQPConnectionError:
            print("Could not connect to RabbitMQ. Retrying in 2 seconds.")
            time.sleep(2)
