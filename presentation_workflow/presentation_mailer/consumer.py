import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

while True:
    try:

        def process_approval(ch, method, properties, body):
            # Decode the JSON message
            message = json.loads(body.decode())

            # Extract the presenter's details
            presenter_name = message["presenter_name"]
            presenter_email = message["presenter_email"]
            presentation_title = message["title"]

            # Format the email content
            email_subject = "Your presentation has been accepted"
            email_body = f"{presenter_name}, we're happy to tell you that your presentation {presentation_title} has been accepted"
            email_from = "admin@conference.go"

            # Send the email
            send_mail(
                email_subject,
                email_body,
                email_from,
                [presenter_email],
                fail_silently=False,
            )

            print(f"Email sent to {presenter_email}")

        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()

        # Declare the 'presentation_approvals' queue
        channel.queue_declare(queue="presentation_approvals")

        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_approval,
            auto_ack=True,
        )

        def process_rejection(ch, method, properties, body):
            # Decode the JSON message
            message = json.loads(body.decode())

            # Extract the presenter's details
            presenter_name = message["presenter_name"]
            presenter_email = message["presenter_email"]
            presentation_title = message["title"]

            # Format the email content
            email_subject = "Your presentation has not been accepted"
            email_body = f"{presenter_name}, we regret to inform you that your presentation {presentation_title} has not been accepted."
            email_from = "admin@conference.go"

            # Send the email
            send_mail(
                email_subject,
                email_body,
                email_from,
                [presenter_email],
                fail_silently=False,
            )

            print(f"Email sent to {presenter_email}")

        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()

        # Declare the 'presentation_rejections' queue
        channel.queue_declare(queue="presentation_rejections")

        channel.basic_consume(
            queue="presentation_rejections",
            on_message_callback=process_rejection,
            auto_ack=True,
        )

        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
