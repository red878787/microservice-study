DEBUG = True
ALLOWED_HOSTS = []
LANGUAGE_CODE = "en-us"
TIME_ZONE = "UTC"
USE_I18N = True
USE_TZ = True

EMAIL_HOST = "mail"
EMAIL_SUBJECT_PREFIX = ""
# EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
# EMAIL_HOST = "localhost"
# EMAIL_PORT = 3000
# EMAIL_HOST_USER = ""
# EMAIL_HOST_PASSWORD = ""
# EMAIL_USE_TLS = False
# DEFAULT_FROM_EMAIL = "admin@conference.go"
